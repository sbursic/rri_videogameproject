﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private Vector2 playerPosition;
    private Vector2 lastPlayerPosition;
    private Rigidbody2D rb;
    public GameObject flame;
    public float moveSpeed = 10f;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (Input.GetKey("up") || Input.GetKey("w"))
        {
            playerPosition.y += moveSpeed * Time.deltaTime;

        }

        if (Input.GetKey("down") || Input.GetKey("s"))
        {
            playerPosition.y -= moveSpeed * Time.deltaTime;

        }

        if (Input.GetKey("left") || Input.GetKey("a"))
        {
            playerPosition.x -= moveSpeed * Time.deltaTime;

        }

        if (Input.GetKey("right") || Input.GetKey("d"))
        {
            playerPosition.x += moveSpeed * Time.deltaTime;

        }

        transform.position = playerPosition;

        if (lastPlayerPosition != playerPosition)
        {
            flame.SetActive(true);
        }
        else flame.SetActive(false);

        lastPlayerPosition = playerPosition;


    }


}
