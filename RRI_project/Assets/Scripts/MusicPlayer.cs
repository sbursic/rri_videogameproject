﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{

    private static MusicPlayer instance = null;
    public static MusicPlayer Instance
    {
        get { return Instance; }
    }
    // Start is called before the first frame update
    void Awake()
    {
        if (instance!= null && instance!= this)
        {
            Destroy(this.gameObject);
            return;
        }

        else
        { instance = this; }
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
