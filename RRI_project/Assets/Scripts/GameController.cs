﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Runtime.CompilerServices;

public class GameController : MonoBehaviour
{
    
    public string nextScene;
    public Text winningText;
    public float enemyMovement = -1;
    public static GameController instance;
    private int score = 0;
    public Text scoreText;
    public int numberOfCrystals=9;
    private bool levelComplete = false;
    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    void Awake()
    {
        staticVariables.PreviousScene = SceneManager.GetActiveScene().name;
        //Debug.Log(staticVariables.PreviousScene);

        //If we don't currently have a game control...
        if (instance == null)
            //...set this one to be it...
            instance = this;
        //...otherwise...
        else if (instance != this)
            //...destroy this one because it is a duplicate.
            Destroy(gameObject);
    }

    public void AddScore()
    {
        score++;
    }

    public void YouLose()
    {
        SceneManager.LoadScene("TryAgain");

    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = "Crystals destroyed : "+score.ToString() +"/"+numberOfCrystals.ToString();

        if (score== numberOfCrystals)
        {
            winningText.gameObject.SetActive(true);
            levelComplete = true;

           // SceneManager.LoadScene("NextLevel");

        }

        if (levelComplete == true)
        {
            if (Input.GetKey("space"))
            {
                SceneManager.LoadScene(nextScene);
            }
        }


    }

    public void StartGame()
    {
        SceneManager.LoadScene("LevelOneStart");
    }

   
}
