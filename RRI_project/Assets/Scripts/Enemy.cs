﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int maxHP=2;
    private int currentHP;
    public bool countScore;
    // Start is called before the first frame update
    void Start()
    {
        currentHP = maxHP;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void loseHP()
    {
        currentHP--;
        if(currentHP<1)
        {
            if (countScore == true)
            {
                GameController.instance.AddScore();
                SoundEffects.PlaySoundEffect("crystal");

            }

            else
            {
                SoundEffects.PlaySoundEffect("wall");
            }
            Destroy(gameObject);
            
        }

    }

    public void gainHP()
    {
        if (currentHP != maxHP)
        {
            currentHP++;
        }
        
    }

   public void Die ()
    {
        Destroy(gameObject);


    }
}
