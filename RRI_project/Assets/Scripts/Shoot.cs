﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Shoot : MonoBehaviour
{

    public Transform firePoint;
    public GameObject bulletPrefab;
    public GameObject blueBulletPrefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("k"))
        {
            ShootBullet();
            SoundEffects.PlaySoundEffect("fire");
        }

        if (SceneManager.GetActiveScene().name != "LevelOne") { 

        if (Input.GetKeyDown("l"))
        {
            ShootBulletBlue();
            SoundEffects.PlaySoundEffect("fire");
            }
    }

}

    void ShootBullet()
    {
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    }


    void ShootBulletBlue()
    {
        Instantiate(blueBulletPrefab, firePoint.position, firePoint.rotation);
    }
}
