﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEscaped : MonoBehaviour
{
    public Camera MainCamera; 
    private Vector2 screenBounds;
    private float objectWidth;
    private float objectHeight;
    // Start is called before the first frame update
    void Start()
    {
        screenBounds = MainCamera.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, MainCamera.transform.position.z));

    }

    // Update is called once per frame
    void Update()
    {
        if(gameObject.transform.position.y < screenBounds.y-11)
        {
            GameController.instance.YouLose();
        }
        
        
    }
}
