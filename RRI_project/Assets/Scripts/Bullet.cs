﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed = 20f;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * speed;
        
    }

    // Update is called once per frame
    void Update()
    {
        
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Enemy enemy= collision.GetComponent<Enemy>();
        EnemyRed enemyGet = collision.GetComponent<EnemyRed>();
        if (enemy!= null)
        {
            enemy.loseHP();
        }

        if (enemyGet != null)
        {
            enemyGet.gainHPRed();
        }

        Destroy(gameObject);
    }
}

