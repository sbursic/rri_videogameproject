﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffects : MonoBehaviour
{
    public static AudioClip fireSound,crystalSound,wallSound;
    static AudioSource audioSource;


    private void Start()
    {
        crystalSound = Resources.Load<AudioClip>("crystalSound");
        fireSound = Resources.Load<AudioClip>("fireSound");
        wallSound = Resources.Load<AudioClip>("wallSound");
        audioSource = GetComponent<AudioSource>();
    }

    public static void PlaySoundEffect(string sound)
    {
        // easy to add new sound effects;
        if (sound=="fire")
        {
            audioSource.PlayOneShot(fireSound);
        }

        if (sound == "crystal")
        {
            audioSource.PlayOneShot(crystalSound);
        }

        if (sound == "wall")
        {
            audioSource.PlayOneShot(wallSound);
        }
    }
}
