﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMoving : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private float movementSpeed=-1.8f;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
         rb2d.velocity = new Vector2(0,movementSpeed);   // Maknit hardkodirano
        

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
