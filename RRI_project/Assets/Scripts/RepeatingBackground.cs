﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackground : MonoBehaviour
{
    private BoxCollider2D sceneCollider;
    private float sceneVerticalLenght;
    // Start is called before the first frame update
    void Start()
    {
        sceneCollider = GetComponent<BoxCollider2D>();
        //sceneVerticalLenght = sceneCollider.size.y;
        sceneVerticalLenght = sceneCollider.bounds.size.x;
        Debug.Log(sceneVerticalLenght);


    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x < -sceneVerticalLenght)   // could be wrong
        {
            RepositionBackground();
        }

       // if (transform.position.y < -sceneVerticalLenght)   // could be wrong
        //{
         //   RepositionBackground();
       // }

    }

    private void RepositionBackground()
    {
        Vector2 backgroundOffset = new Vector2(sceneVerticalLenght * 2f,0);
        transform.position = (Vector2)transform.position + backgroundOffset;
    }
}
