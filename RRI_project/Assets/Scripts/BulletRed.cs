﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletRed : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 20f;
    private Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.up * speed;

    }

    // Update is called once per frame
    void Update()
    {


    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        EnemyRed enemy = collision.GetComponent<EnemyRed>();
        Enemy enemyGet = collision.GetComponent<Enemy>();
        if (enemy != null)
        {
            enemy.loseHPRed();
        }
        
        if (enemyGet != null)
        {
            enemyGet.gainHP();
        }
        Destroy(gameObject);

    }
}
