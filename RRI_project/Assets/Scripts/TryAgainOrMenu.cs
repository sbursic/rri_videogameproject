﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TryAgainOrMenu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("space"))
        {
            SceneManager.LoadScene(staticVariables.PreviousScene);
        }

        if(Input.GetKey("escape"))
        {
            SceneManager.LoadScene("MainMenu");
        }
        
    }
}
